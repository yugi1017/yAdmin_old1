import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/view/main/main.vue'

Vue.use(Router)

export default new Router(
  {
    mode: 'history',
    routes: [
      {
        path: '/',
        name: 'home',
        redirect: '/home',
        component: Main,
        children: [
          { path: 'home', title: '首页', name: 'home_index', component: resolve => { require(['@/view/home/home.vue'], resolve); } },
        ]
      },
      {
        path: '/view',
        name: 'view',        
        component: Main,
        children: [
          { path: 'listPage', title: '分页表格', name: 'listPage_index', component: resolve => { require(['@/view/listPage/listPage.vue'], resolve); } },
          { path: 'formPage', title: '表单页', name: 'formPage_index', component: resolve => { require(['@/view/formPage/formPage.vue'], resolve); } },
          { path: 'draggablePage', title: '拖拽页', name: 'draggablePage_index', component: resolve => { require(['@/view/draggablePage/draggablePage.vue'], resolve); } },
        ]
      }
    ]
  },
)
