const state = {
    menutagList: [{ key: '/', name: '首页', cur: true }],
    curTag: { key: '/', name: '首页', cur: true },
}
const getters = {

}
const actions = {

}
const mutations = {
    addMenuTags(state, tag) {

        var menutagList = state.menutagList;
        var isAdd = true;
        for (var i = 0; i < menutagList.length; i++) {
            var tagItem = menutagList[i];
            if (tag.key == tagItem.key) {
                isAdd = false;
                state.curTag = tagItem;
            }
        }
        if (isAdd) {            
            menutagList.push(tag);
            state.curTag = tag;
        }
    },
    pickTag(state, tag) {        
        state.curTag = tag;
    },
    closeTag(state, key) {        
        var menutagList = state.menutagList;        
        for (var i = 0; i < menutagList.length; i++) {
            var tagItem = menutagList[i];            
            if (key == tagItem.key) {
                menutagList.splice(i, 1);
                var preInt = i-1;
                if(preInt<0){
                    preInt = 0;
                }
                if(key==state.curTag.key){
                    state.curTag = menutagList[preInt];
                }
                break;
            }
        }

    }
}


export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}