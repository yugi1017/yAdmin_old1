import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import router from './router'
import store from './store'

import contentmenu from 'v-contextmenu'
import 'v-contextmenu/dist/index.css'

Vue.use(ElementUI)
Vue.use(contentmenu)

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
  
})
