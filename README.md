# yadmin

> 使用vue,elementui自己搭建的后台框架，用于项目后台的更新框架



## Develop

``` bash
# serve with hot reload at localhost:8010
npm run dev
```

## Build

``` bash
# build for production with minification
npm run build
```
